package com.release5.exercices.ex5;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;

public class Tab5 extends HorizontalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	public Tab5() {

		setStyleAndLayout();

		String url = "https://dev.vaadin.com/svn/doc/book-examples/branches/vaadin-7/src/com/vaadin/book/examples/application/calc/";

		Link link = new Link("Solution", new ExternalResource(url));

		this.addComponent(link);

	}

	private void setStyleAndLayout() {
		this.setMargin(true);
	}

}
