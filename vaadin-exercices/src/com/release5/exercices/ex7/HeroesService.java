package com.release5.exercices.ex7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.release5.exercices.ex7.model.Group;
import com.release5.exercices.ex7.model.Superhero;

class HeroesService implements Serializable {

	/** IDE. */
	private static final long serialVersionUID = -1079978228547611403L;

	private List<Group> groups = new ArrayList<Group>();

	private static int counter = 10000;

	HeroesService() {

		Group heroes = new Group(1, "Heroes");

		groups.add(heroes);

		Superhero superman = new Superhero(1000, false, heroes, "Superman");
		heroes.addSuperhero(superman);

		Superhero batman = new Superhero(1001, false, heroes, "Batman");
		heroes.addSuperhero(batman);

		Superhero spiderMan = new Superhero(1002, false, heroes, "Spider-Man");
		heroes.addSuperhero(spiderMan);

		Group avengers = new Group(200, "Avengers", heroes);
		heroes.addChild(avengers);

		Superhero captainAmerica = new Superhero(2000, false, avengers,
				"Captain America");
		avengers.addSuperhero(captainAmerica);

		Superhero ironMan = new Superhero(2001, false, avengers, "Iron Man");
		avengers.addSuperhero(ironMan);

		Superhero blackWidow = new Superhero(2002, false, avengers,
				"Black Widow");
		avengers.addSuperhero(blackWidow);

		Superhero thor = new Superhero(2003, false, avengers, "Thor");
		avengers.addSuperhero(thor);

		Group fantasticFour = new Group(300, "Fantastic Four", heroes);
		heroes.addChild(fantasticFour);

		Superhero misterFantastic = new Superhero(3000, false, fantasticFour,
				"Mister Fantastic");
		fantasticFour.addSuperhero(misterFantastic);

		Superhero invisibleWoman = new Superhero(3001, false, fantasticFour,
				"Invisible Woman");
		fantasticFour.addSuperhero(invisibleWoman);

		Superhero thing = new Superhero(3002, false, fantasticFour, "Thing");
		fantasticFour.addSuperhero(thing);

		Superhero humanTorch = new Superhero(3003, false, fantasticFour,
				"Human Torch");
		fantasticFour.addSuperhero(humanTorch);

		Group xMen = new Group(400, "X-Men", heroes);
		heroes.addChild(xMen);

		Superhero professorX = new Superhero(4000, false, xMen, "professor X");
		xMen.addSuperhero(professorX);

		Superhero wolverine = new Superhero(4001, false, xMen, "Wolverine");
		xMen.addSuperhero(wolverine);

		Group villain = new Group(5, "Villain");
		groups.add(villain);

		Superhero magneto = new Superhero(5000, false, villain, "Magneto");
		villain.addSuperhero(magneto);

		Superhero jocker = new Superhero(5001, false, villain, "Jocker");
		villain.addSuperhero(jocker);

		Group legionOfDoom = new Group(600, "Legion of Doom", villain);
		villain.addChild(legionOfDoom);

		Superhero brainiac = new Superhero(6000, false, legionOfDoom,
				"Brainiac");
		legionOfDoom.addSuperhero(brainiac);

		Superhero scarecrow = new Superhero(6001, false, legionOfDoom,
				"Scarecrow");
		legionOfDoom.addSuperhero(scarecrow);

		Superhero sinestro = new Superhero(6002, false, legionOfDoom,
				"Sinestro");
		legionOfDoom.addSuperhero(sinestro);

	}

	List<Group> getHeroes() {
		return groups;
	}

	boolean isGroup(Integer groupId) {

		return getGroup(groupId) != null;

	}

	Superhero addSuperhero(Integer groupId, String name, boolean isVilain) {

		Group group = getGroup(groupId);

		Superhero newHero = new Superhero(counter, isVilain, group, name);

		counter = counter + 1;

		group.addSuperhero(newHero);

		return newHero;
	}

	private Group getGroup(Integer groupId) {

		for (Group group : groups) {

			if (group.getId() == groupId) {

				return group;
			}

			List<Group> childGroups = group.getChildGroups();

			for (Group childGroup : childGroups) {

				if (childGroup.getId() == groupId) {

					return childGroup;
				}

			}

		}

		return null;

	}

}
