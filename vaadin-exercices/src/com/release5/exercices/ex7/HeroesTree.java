package com.release5.exercices.ex7;

import java.util.List;

import com.release5.exercices.ex7.model.Superhero;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;

class HeroesTree extends Tree implements Handler {

	/** IDE */
	private static final long serialVersionUID = -34674734835938168L;

	private HeroHierarchicalContainer hierarchicalContainer = new HeroHierarchicalContainer();

	private static Action DELETE_ACTION = new Action("Supprimer",
			FontAwesome.MINUS_CIRCLE);

	private static Action ADD_ACTION = new Action("Ajouter",
			FontAwesome.PLUS_CIRCLE);

	private Integer selectedGroupId;

	HeroesTree() {

		this.setContainerDataSource(hierarchicalContainer);

		addDeleteOption();

		setCaption();

		this.setImmediate(true);

	}

	private void addDeleteOption() {

		this.addActionHandler(this);
	}

	private void setCaption() {

		List<Integer> itemIds = (List<Integer>) hierarchicalContainer
				.getItemIds();

		for (Integer itemId : itemIds) {

			Item item = hierarchicalContainer.getItem(itemId);

			Property<?> captionProperty = item.getItemProperty("caption");

			String caption = (String) captionProperty.getValue();

			this.setItemCaption(itemId, caption);

		}
	}

	@Override
	public Action[] getActions(Object target, Object sender) {

		if (target != null && target instanceof Integer) {

			Integer id = (Integer) target;

			if (!hierarchicalContainer.isGroup(id)) {

				return new Action[] { DELETE_ACTION };
			}

		}

		return new Action[] { ADD_ACTION, DELETE_ACTION };

	}

	@Override
	public void handleAction(Action action, Object sender, Object target) {

		if (action == DELETE_ACTION && target instanceof Integer) {

			Integer id = (Integer) target;

			hierarchicalContainer.removeItem(id);

		} else if (action == ADD_ACTION && target instanceof Integer) {

			selectedGroupId = (Integer) target;

			AddHeroWindow addHeroWindow = new AddHeroWindow(this);

			UI.getCurrent().addWindow(addHeroWindow);

		}

	}

	public void addHero(String name, boolean isVilain) {

		Superhero hero = hierarchicalContainer.addHero(selectedGroupId, name,
				isVilain);

		this.setItemCaption(hero.getId(), hero.getName());

	}

}
