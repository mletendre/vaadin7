package com.release5.exercices.ex2;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;

public class Tab2 extends GridLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	public Tab2() {

		super(3, 3);
		
		setStyleAndLayout();

		Button a1 = new Button("A1");
		Button a2 = new Button("A2");
		Button a3 = new Button("A3");

		Label b1 = new Label("B1");
		Label b2 = new Label("B2");
		Label b3 = new Label("B3");

		Button c1 = new Button("C1");
		Button c2 = new Button("C2");
		NativeButton c3 = new NativeButton("C3");

		addComponent(a1, 0, 0);
		addComponent(a2, 1, 0);
		addComponent(a3, 2, 0);
		addComponent(b1, 0, 1);
		addComponent(b2, 1, 1);
		addComponent(b3, 2, 1);
		addComponent(c1, 0, 2);
		addComponent(c2, 1, 2);
		addComponent(c3, 2, 2);

		setColumnExpandRatio(0, 1);
		setColumnExpandRatio(1, 1);
		setColumnExpandRatio(2, 2);

		// A1
		a1.setWidth(100, Unit.PERCENTAGE);

		// A2
		setComponentAlignment(a2, Alignment.MIDDLE_CENTER);

		// A3
		setComponentAlignment(a3, Alignment.TOP_RIGHT);

		// B2
		b2.setSizeUndefined();
		setComponentAlignment(b2, Alignment.MIDDLE_CENTER);

		// B3
		b3.setSizeUndefined();
		setComponentAlignment(b3, Alignment.BOTTOM_CENTER);

		// C1
		setComponentAlignment(c1, Alignment.BOTTOM_RIGHT);

		// C2
		c2.setWidth(75, Unit.PERCENTAGE);

		// C3
		c3.setSizeFull();

	}

	private void setStyleAndLayout() {
		
		setHeight(500, Unit.PIXELS);
		
		setWidth(500, Unit.PIXELS);
		
	}

}
