package com.release5.exercices.ex11;

import com.vaadin.ui.HorizontalLayout;

public class Tab11 extends HorizontalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	private CaptchaField captchaField = new CaptchaField();

	public Tab11() {

		setStyleAndLayout();

		this.addComponent(captchaField);

	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);

	}

	public void reloadCaptcha() {
		
		captchaField.reloadCaptcha();
	}

}
