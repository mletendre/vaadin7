package com.release5.exercices.ex3;

import com.release5.exercices.ExerciesTabSheet;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class Tab3 extends GridLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	private TextField firstNamefield = new TextField();

	private TextField lastNamefield = new TextField();

	private ExerciesTabSheet exerciesTabSheet;

	public Tab3(ExerciesTabSheet exerciesTabSheet) {

		super(4, 7);

		this.exerciesTabSheet = exerciesTabSheet;

		setStyleAndLayout();

		addLeftSubtitle();

		addFirstName();

		addLastName();

		addEmail();

		addGender();

		addActivate();

		addRightSubtitle();

		addDepartment();

		addHireDate();

		addComment();

		addButtons();

	}

	private void addLeftSubtitle() {
		final int ROW = 0;
		Label label = new Label("Description");
		addComponent(label, 0, ROW);
	}

	private void addFirstName() {

		final int ROW = 1;
		Label label = new Label("Prénom:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		firstNamefield.setWidth(250, Unit.PIXELS);
		addComponent(firstNamefield, 1, ROW);
	}

	private void addLastName() {
		final int ROW = 2;
		Label label = new Label("Nom:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		lastNamefield.setWidth(250, Unit.PIXELS);
		addComponent(lastNamefield, 1, ROW);
	}

	private void addEmail() {
		final int ROW = 3;

		Label label = new Label("Courriel:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		TextField field = new TextField();
		field.setWidth(250, Unit.PIXELS);
		addComponent(field, 1, ROW);

	}

	private void addGender() {
		final int ROW = 4;

		Label label = new Label("Sexe:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		OptionGroup optionGroup = new OptionGroup();
		addComponent(optionGroup, 1, ROW);

		optionGroup.addItem("Masculin");
		optionGroup.setItemIcon("Masculin", FontAwesome.MALE);
		optionGroup.addItem("Féminin");
		optionGroup.setItemIcon("Féminin", FontAwesome.FEMALE);

		optionGroup.select("Masculin");

	}

	private void addActivate() {

		final int ROW = 5;

		Label label = new Label("Actif:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		CheckBox checkBox = new CheckBox();
		checkBox.setValue(true);
		addComponent(checkBox, 1, ROW);
	}

	private void addRightSubtitle() {
		final int ROW = 0;
		Label label = new Label("Profile Corporatif");
		addComponent(label, 2, ROW);
	}

	private void addDepartment() {

		final int ROW = 1;
		Label label = new Label("Departement:");
		label.setSizeUndefined();
		addComponent(label, 2, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		ComboBox comboBox = new ComboBox();
		comboBox.addItem("Finance");
		comboBox.addItem("Achats");
		comboBox.addItem("Ventes");
		comboBox.addItem("TI");
		comboBox.select("Finance");
		comboBox.setNullSelectionAllowed(false);
		addComponent(comboBox, 3, ROW);

	}

	private void addHireDate() {

		final int ROW = 2;

		Label label = new Label("Date d'embauche:");
		label.setSizeUndefined();
		addComponent(label, 2, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		PopupDateField popupDateField = new PopupDateField();
		addComponent(popupDateField, 3, ROW);

	}

	private void addComment() {

		final int ROW = 3;

		Label label = new Label("Commentaire:");
		label.setSizeUndefined();
		addComponent(label, 2, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		TextArea commentField = new TextArea();
		commentField.setSizeFull();
		addComponent(commentField, 3, ROW);

	}

	private void addButtons() {

		final int ROW = 4;

		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		this.addComponent(buttonLayout, 3, ROW);
		this.setComponentAlignment(buttonLayout, Alignment.TOP_RIGHT);

		addClearButton(buttonLayout);

		addSaveButton(buttonLayout);

	}

	private void addSaveButton(HorizontalLayout buttonLayout) {

		Button button = new Button("Save");
		buttonLayout.addComponent(button);

		button.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = -1191259691313136437L;

			@Override
			public void buttonClick(ClickEvent event) {

				String message = firstNamefield.getValue() + " "
						+ lastNamefield.getValue() + " a été ajouté";

				Notification.show(message, Type.HUMANIZED_MESSAGE);

			}
		});

	}

	private void addClearButton(HorizontalLayout buttonLayout) {

		Button button = new Button("Clear");
		buttonLayout.addComponent(button);

		final Tab3 oldTab = this;

		button.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = 6278371622311282007L;

			@Override
			public void buttonClick(ClickEvent event) {

				Tab3 newTab = new Tab3(exerciesTabSheet);

				exerciesTabSheet.replaceComponent(oldTab, newTab);

			}
		});

	}

	private void setStyleAndLayout() {

		setWidth(100, Unit.PERCENTAGE);

		setSpacing(true);

		setMargin(true);
	}

}
