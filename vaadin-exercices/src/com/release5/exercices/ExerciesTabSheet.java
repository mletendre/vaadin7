package com.release5.exercices;

import com.release5.exercices.ex10.Tab10;
import com.release5.exercices.ex11.Tab11;
import com.release5.exercices.ex2.Tab2;
import com.release5.exercices.ex3.Tab3;
import com.release5.exercices.ex4.Tab4;
import com.release5.exercices.ex5.Tab5;
import com.release5.exercices.ex6.Tab6;
import com.release5.exercices.ex7.Tab7;
import com.release5.exercices.ex8.Tab8;
import com.release5.exercices.ex9.Tab9;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.TabSheet;

public class ExerciesTabSheet extends TabSheet {

	/** IDE. */
	private static final long serialVersionUID = -9206103330419895918L;

	private Tab11 tab11 = new Tab11();

	public ExerciesTabSheet() {

		setStyleAndLayout();

		this.addTab(new Tab2(), "Exercice 2", FontAwesome.ALIGN_RIGHT);
		this.addTab(new Tab3(this), "Exercice 3", FontAwesome.LIST_ALT);
		this.addTab(new Tab4(this), "Exercice 4", FontAwesome.EDIT);
		this.addTab(new Tab5(), "Exercice 5", FontAwesome.EXTERNAL_LINK);
		this.addTab(new Tab6(), "Exercice 6", FontAwesome.TABLE);
		this.addTab(new Tab7(), "Exercice 7", FontAwesome.STAR);
		this.addTab(new Tab8(), "Exercice 8", FontAwesome.QRCODE);
		this.addTab(new Tab9(), "Exercice 9", FontAwesome.PICTURE_O);
		this.addTab(new Tab10(), "Exercice 10", FontAwesome.ARROWS_ALT);
		this.addTab(tab11, "Exercice 11", FontAwesome.CUBES);

	}

	private void setStyleAndLayout() {

		this.setSizeFull();

	}

	public void reloadCaptcha() {
		
		tab11.reloadCaptcha();

	}

}
