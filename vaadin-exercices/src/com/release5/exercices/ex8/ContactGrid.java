package com.release5.exercices.ex8;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.release5.exercices.ex8.model.Contact;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TextField;

class ContactGrid extends Grid {

	/** IDE. */
	private static final long serialVersionUID = -5564109594633312982L;

	private BeanItemContainer<Contact> contactContainer = new BeanItemContainer<Contact>(Contact.class);

	private final ContactService contactService = new ContactService();

	ContactGrid() {

		setData();

	}

	private void setData() {

		List<Contact> contacts = contactService.getContacts();

		// todo

	}

}
