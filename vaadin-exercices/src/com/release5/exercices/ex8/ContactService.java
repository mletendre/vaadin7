package com.release5.exercices.ex8;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.release5.exercices.ex8.model.Contact;

public class ContactService implements Serializable {

	public List<Contact> getContacts() {
		List<Contact> contacts = new ArrayList<Contact>();

		Contact contact1 = new Contact("Wilhelmina", "Murray", "514-900-8766",
				"wilhelmina.Murray@hotmail.com");
		contacts.add(contact1);

		Contact contact2 = new Contact("Captain", "Nemo", "514-254-8745",
				"captain.nemo@gmail.com");
		contacts.add(contact2);

		Contact contact3 = new Contact("Allan", "Quatermain", "514-624-8965",
				"allan.quatermain@gmail.com");
		contacts.add(contact3);

		Contact contact4 = new Contact("Henry", "Jekyll", "514-963-2587",
				"henry.jekyll@outlook.com");
		contacts.add(contact4);

		Contact contact5 = new Contact("Hawley", "Griffin", "514-524-9614",
				"hawley.griffin@outlook.com");
		contacts.add(contact5);

		Contact contact6 = new Contact("Lemuel", "Gulliver", "514-888-9632",
				"lemuel.gulliver@gmail.com");
		contacts.add(contact6);

		Contact contact7 = new Contact("Christopher", "Syn", "514-222-9632",
				"christopher.syn@gmail.com");
		contacts.add(contact7);

		Contact contact8 = new Contact("Marguerite", "Blakeney",
				"514-741-3654", "marguerite.blakeney@gmail.com");
		contacts.add(contact8);

		Contact contact9 = new Contact("Nathanael", "Bumppo", "514-453-9514",
				"nathanael.bumppo@outlook.com");
		contacts.add(contact9);

		Contact contact10 = new Contact("Thomas", "Carnacki", "514-452-7777",
				"thomas.carnacki@outlook.com");
		contacts.add(contact10);

		Contact contact11 = new Contact("Jean", "Robur", "514-665-8852",
				"Jean.Robur@outlook.com");
		contacts.add(contact11);

		Contact contact12 = new Contact("Arsène", "Lupin", "514-455-4566",
				"Arsene.Lupin@outlook.com");
		contacts.add(contact12);

		Contact contact13 = new Contact("Nyctalope", "", "514-885-7733",
				"Nyctalope@gmail.com");
		contacts.add(contact13);

		Contact contact14 = new Contact("Fantômas", "", "514-228-9944",
				"Fantomas@outlook.com");
		contacts.add(contact14);

		Contact contact15 = new Contact("Monsieur", "Zenith", "514-523-8855",
				"Monsieur.Zenith@gmail.com");
		contacts.add(contact15);

		return contacts;
	}

}
