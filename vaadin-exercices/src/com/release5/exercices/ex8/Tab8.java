package com.release5.exercices.ex8;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

public class Tab8 extends HorizontalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	public Tab8() {

		setStyleAndLayout();

		ContactGrid contactGrid = new ContactGrid();

		this.addComponent(contactGrid);
		
		MenuBar menuBar = new MenuBar();
		
		MenuItem bars = menuBar.addItem("", FontAwesome.BARS , null);

		bars.addItem("ADD", FontAwesome.PLUS_CIRCLE , null);
		
	}

	private void setStyleAndLayout() {

		this.setSpacing(true);

		this.setMargin(true);

		this.setWidth(100, Unit.PERCENTAGE);
	}

}
