package com.release5.exercices.ex6;

import java.io.Serializable;

public class Account implements Serializable {

	/** IDE. */
	private static final long serialVersionUID = 8372769815447931656L;

	private String number;

	// Can be "CAD" or "USD"
	private String currency;

	private long currentBalance;

	private long previousBalance;

	public Account(String number, String currency, long currentBalance,
			long previousBalance) {
		super();
		this.number = number;
		this.currency = currency;
		this.currentBalance = currentBalance;
		this.previousBalance = previousBalance;
	}

	// Getters and setters....

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public long getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(long currentBalance) {
		this.currentBalance = currentBalance;
	}

	public long getPreviousBalance() {
		return previousBalance;
	}

	public void setPreviousBalance(long previousBalance) {
		this.previousBalance = previousBalance;
	}

}
