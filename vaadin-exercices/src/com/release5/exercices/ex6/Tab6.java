package com.release5.exercices.ex6;

import com.vaadin.ui.VerticalLayout;

public class Tab6 extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	public Tab6() {

		setStyleAndLayout();

		AccountTable accountTable = new AccountTable();

		this.addComponent(accountTable);

	}

	private void setStyleAndLayout() {
		
		this.setMargin(true);
	
	}

}
