package com.release5.exercices;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

class ContentLayout extends GridLayout {

	/** IDE. */
	private static final long serialVersionUID = -3086530263246370094L;

	private static final ThemeResource LOGO_ICON = new ThemeResource("img/logo.png");

	private ExerciesTabSheet exercicesTabSheet = new ExerciesTabSheet();

	private Label title = new Label("Formation Vaadin 7");

	ContentLayout() {

		super(2, 2);

		setStyleAndLayout();

		addComponent(new Image(null, LOGO_ICON), 0, 0);

		addTitle();

		addComponent(exercicesTabSheet, 0, 1, 1, 1);
	}

	private void addTitle() {

		this.addComponent(title, 1, 0);

		title.setSizeUndefined();

		this.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
	}

	private void setStyleAndLayout() {

		setSpacing(true);

		setSizeFull();

		setRowExpandRatio(0, 0);

		setRowExpandRatio(1, 1);

		setColumnExpandRatio(0, 0);

		setColumnExpandRatio(1, 1);

	}

	public void reset() {

		exercicesTabSheet.reloadCaptcha();

	}
}
