package com.release5.exercices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.Page;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.SystemMessages;
import com.vaadin.server.SystemMessagesInfo;
import com.vaadin.server.SystemMessagesProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("theme")
public class VaadinExercicesUI extends UI {

	private ContentLayout content = new ContentLayout();

	@Override
	protected void init(VaadinRequest request) {

		setContent(content);

		Page.getCurrent().setTitle("Formation Vaadin 7 - Release 5");

		addShortchutListener();

	}

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = VaadinExercicesUI.class, heartbeatInterval = 10)
	public static class Servlet extends VaadinServlet {

		@Override
		protected void servletInitialized() throws ServletException {

			super.servletInitialized();

			getService().addSessionInitListener(new SessionInitListener() {
				@Override
				public void sessionInit(SessionInitEvent event) throws ServiceException {
					event.getSession().addUIProvider(new ExercicesUIProvider());
				}
			});

			getService().setSystemMessagesProvider(new SystemMessagesProvider() {
				@Override
				public SystemMessages getSystemMessages(SystemMessagesInfo systemMessagesInfo) {
					CustomizedSystemMessages messages = new CustomizedSystemMessages();
					messages.setCommunicationErrorCaption("Comm Err");
					messages.setCommunicationErrorMessage("Problème de communication.");
					messages.setCommunicationErrorNotificationEnabled(true);
					messages.setCommunicationErrorURL("http://vaadin.com/");
					return messages;
				}
			});
		}

	}

	private void addShortchutListener() {

		ShortcutListener shortcutListener = new ShortcutListener("down", KeyCode.R, new int[] { ModifierKey.ALT }) {

			@Override
			public void handleAction(Object sender, Object target) {

				Notification.show("Reload Captcha !");

				content.reset();

			}

		};

		addShortcutListener(shortcutListener);
	}

}