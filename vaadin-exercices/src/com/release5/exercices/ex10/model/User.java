package com.release5.exercices.ex10.model;

import java.io.Serializable;

public class User implements Serializable {

	/** IDE. */
	private static final long serialVersionUID = -3231451675586295976L;

	private String name;

	private String jobTitle;

	public User(String name, String jobTitle) {
		super();
		this.name = name;
		this.jobTitle = jobTitle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

}
