package com.release5.exercices.ex10;

import java.util.Iterator;
import java.util.List;

import com.release5.exercices.ex10.model.User;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

public class UserLayout extends HorizontalLayout {

	/** IDE. */
	// extends DragAndDropWrapper
	private static final long serialVersionUID = -503713068307613659L;

	public UserLayout(List<User> users) {

		this();

		setStyleAndLayout();

		if (users != null) {

			addUsers(users);

		}

	}

	public UserLayout() {

		setStyleAndLayout();
	}

	private void addUsers(List<User> users) {

		for (User user : users) {

			addUser(user);
		}
	}

	void addUser(User user) {

		UserCardDropWrapper findUserCardDropWrapper = findUserCardDropWrapper(user);

		if (findUserCardDropWrapper == null) {

			UserCardDropWrapper userCard = new UserCardDropWrapper(user);

			this.addComponent(userCard);

			this.setComponentAlignment(userCard, Alignment.MIDDLE_LEFT);

			this.setExpandRatio(userCard, 0);
		}

	}

	public void removeUser(User user) {

		UserCardDropWrapper userCardDropWrapper = findUserCardDropWrapper(user);

		if (userCardDropWrapper != null) {

			this.removeComponent(userCardDropWrapper);

		}
	}

	public UserCardDropWrapper findUserCardDropWrapper(User user) {

		Iterator<Component> iterator = this.iterator();

		while (iterator.hasNext()) {

			Component next = iterator.next();

			if (next instanceof UserCardDropWrapper) {

				UserCardDropWrapper userCardDropWrapper = (UserCardDropWrapper) next;

				User currentUser = userCardDropWrapper.getUser();

				if (currentUser == user) {

					return userCardDropWrapper;
				}
			}

		}
		return null;
	}

	private void setStyleAndLayout() {

		this.setMargin(true);

		this.setSpacing(true);

		this.setHeight(200, Unit.PIXELS);

	}

}
