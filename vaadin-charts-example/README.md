vaadin-charts-example
==============

A demo of the Vaadin Charts library.

Run the app.
========

1. Install maven.
2. Get a temporay licence for Vaadin Charts https://vaadin.com/pro/licenses\
3. Copy the licence (vaadin.charts.developer.license) under /vaadin-charts-example/src/main/resources
4. To compile the entire project, run "mvn install".
5. To run the application, run "mvn jetty:run" and open http://localhost:8080/ .



