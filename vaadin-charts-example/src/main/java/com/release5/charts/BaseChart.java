package com.release5.charts;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.Legend;
import com.vaadin.addon.charts.model.ListSeries;
import com.vaadin.addon.charts.model.XAxis;
import com.vaadin.addon.charts.model.YAxis;
import com.vaadin.addon.charts.model.style.SolidColor;
import com.vaadin.ui.VerticalLayout;

class BaseChart extends VerticalLayout {

	BaseChart() {

		this.setMargin(true);

		setSizeFull();

		Chart chart = new Chart(ChartType.COLUMN);
		chart.setWidth(400, Unit.PIXELS);
		chart.setHeight(300, Unit.PIXELS);

		Configuration conf = chart.getConfiguration();
		conf.setTitle("Résultats scolaires");
		conf.setSubTitle("Dictées");

		ListSeries seriesA = new ListSeries("John");
		seriesA.setData(2, 1, 2, 4, 6);
		conf.addSeries(seriesA);

		ListSeries seriesB = new ListSeries("Bob");
		seriesB.setData(10, 10, 9, 10, 8);
		conf.addSeries(seriesB);

		XAxis xaxis = new XAxis();
		xaxis.setCategories("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi");
		xaxis.setTitle("Jours");
		conf.addxAxis(xaxis);

		YAxis yaxis = new YAxis();
		yaxis.setMax(10);
		yaxis.setTitle("Score");
		conf.addyAxis(yaxis);
		
		Legend legend = new Legend();
		legend.setEnabled(true);
		legend.setBackgroundColor(SolidColor.AQUAMARINE );
		conf.setLegend(legend);

		addComponent(chart);
	}

}
