package com.release5.charts;

import com.vaadin.ui.TabSheet;

public class ChartTabSheet extends TabSheet {

	/** IDE. */
	private static final long serialVersionUID = 7666534529463797645L;

	public ChartTabSheet() {

		addTab(new BaseChart(), "Base");
		addTab(new Chart3D(), "Pie 3D");

	}

}
