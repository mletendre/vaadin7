package com.release5.charts;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import com.vaadin.addon.charts.model.Legend;
import com.vaadin.addon.charts.model.Options3d;
import com.vaadin.addon.charts.model.style.Color;
import com.vaadin.addon.charts.model.style.SolidColor;
import com.vaadin.ui.VerticalLayout;

class Chart3D extends VerticalLayout {

	Chart3D() {

		this.setMargin(true);

		setSizeFull();

		Chart chart = new Chart(ChartType.PIE);
		chart.setWidth(400, Unit.PIXELS);
		chart.setHeight(300, Unit.PIXELS);

		Configuration configuration = chart.getConfiguration();
		String title = "Pigeons can discriminate good and bad paintings by children";
		configuration.setTitle(title);

		final DataSeries series = new DataSeries();
		series.add(new DataSeriesItem("Good Art: Correct", 18));
		series.add(new DataSeriesItem("Good Art: Error", 22));
		series.add(new DataSeriesItem("Bad Art: Correct", 41));
		series.add(new DataSeriesItem("Bad Art: Error", 19));
		configuration.setSeries(series);
		
		Options3d options3d = new Options3d();
		options3d.setEnabled(true);
		options3d.setAlpha(60);
		configuration.getChart().setOptions3d(options3d);

		addComponent(chart);
	}

}
