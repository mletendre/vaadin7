package com.release5.exercices.ex8;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.release5.exercices.ex8.model.Contact;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TextField;

class ContactGrid extends Grid {

	/** IDE. */
	private static final long serialVersionUID = -5564109594633312982L;

	private BeanItemContainer<Contact> contactContainer = new BeanItemContainer<Contact>(
			Contact.class);

	private final ContactService contactService = new ContactService();

	ContactGrid() {

		setData();

		setColumnStyle();

		addTitle();

		addSearchFilters();

		setStyleAndLayout();

	}

	private void addGridMenu(HeaderRow titleRow) {

		HorizontalLayout layout = new HorizontalLayout();

		layout.setSizeFull();

		MenuBar menuBar = new MenuBar();

		MenuItem bars = menuBar.addItem("", FontAwesome.BARS, null);

		bars.addItem("ADD", FontAwesome.PLUS_CIRCLE, null);

		layout.addComponent(menuBar);

		layout.setComponentAlignment(menuBar, Alignment.TOP_RIGHT);

		HeaderCell cell = titleRow.getCell("email");

		cell.setComponent(layout);

	}

	private void setData() {

		List<Contact> contacts = contactService.getContacts();

		contactContainer.addAll(contacts);

		setContainerDataSource(contactContainer);

	}

	private void setColumnStyle() {

		setColumnOrder("firstName", "lastName", "phone", "email");

		removeColumn("comments");

		Grid.Column lastNameColumn = getColumn("lastName");
		lastNameColumn.setHeaderCaption("Nom");

		Grid.Column firstNameColumn = getColumn("firstName");
		firstNameColumn.setHeaderCaption("Prénom");

		Grid.Column phoneColumn = getColumn("phone");
		phoneColumn.setHeaderCaption("No de téléphone");

		Grid.Column emailColumn = getColumn("email");
		emailColumn.setHeaderCaption("Courriel");

	}

	private void addTitle() {

		HeaderRow titleRow = prependHeaderRow();

		HeaderCell titleCell = titleRow.join("firstName", "lastName", "phone");

		titleCell.setStyleName("myheader");

		titleCell.setText("Carnet d'adresse");

		addGridMenu(titleRow);

	}

	private void addSearchFilters() {

		HeaderRow filterRow = appendHeaderRow();

		HeaderCell searchFilterCell = filterRow.join("firstName", "lastName");

		HorizontalLayout searchLayout = getSearchLayout();

		searchFilterCell.setComponent(searchLayout);

	}

	private HorizontalLayout getSearchLayout() {

		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setSpacing(true);
		searchLayout.setSizeFull();

		Label label = new Label(FontAwesome.SEARCH.getHtml() + " Search:");
		label.setSizeUndefined();
		label.setContentMode(ContentMode.HTML);
		searchLayout.addComponent(label);
		searchLayout.setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		TextField searchField = new TextField();
		searchField.setWidth(100, Unit.PERCENTAGE);

		searchField.addTextChangeListener(new TextChangeListener() {

			/** IDE. */
			private static final long serialVersionUID = 5333104053026050470L;

			@Override
			public void textChange(TextChangeEvent event) {

				String expression = event.getText();

				filterGrid(expression);
			}
		});

		searchLayout.addComponent(searchField);
		
		searchLayout.setComponentAlignment(searchField, Alignment.MIDDLE_LEFT);

		return searchLayout;

	}

	private void filterGrid(String expression) {

		if (StringUtils.isEmpty(expression)) {

			contactContainer.removeAllContainerFilters();

		} else {

			expression = expression.trim();

			SimpleStringFilter firstNameFilter = new SimpleStringFilter(
					"firstName", expression, true, false);

			SimpleStringFilter lastNameFilter = new SimpleStringFilter(
					"lastName", expression, true, false);

			Or orFilter = new Or(firstNameFilter, lastNameFilter);

			contactContainer.addContainerFilter(orFilter);
		}

	}

	private void setStyleAndLayout() {

		setColumnReorderingAllowed(true);

		setWidth(80, Unit.PERCENTAGE);

		setEditorEnabled(true);

		setHeightByRows(8);

		setHeightMode(HeightMode.ROW);

		setEditorSaveCaption("Sauvegarde");
		setEditorCancelCaption("Annuler");

	}

}
