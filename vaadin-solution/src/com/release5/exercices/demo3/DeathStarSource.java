package com.release5.exercices.demo3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.vaadin.server.StreamResource.StreamSource;

public class DeathStarSource implements StreamSource {
	/** IDE. */
	private static final long serialVersionUID = 5102379055134210795L;

	public InputStream getStream() {

		BufferedImage image = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
		Graphics drawable = image.getGraphics();
		drawable.setColor(Color.BLACK);
		drawable.fillRect(0, 0, 200, 200);
		drawable.setColor(Color.lightGray);
		drawable.fillOval(25, 25, 150, 150);
		drawable.setColor(Color.DARK_GRAY);
		drawable.drawRect(0, 0, 199, 199);
		drawable.setColor(Color.WHITE);
		drawable.fillOval(110, 60, 40, 40);

		try {
			ByteArrayOutputStream imagebuffer = new ByteArrayOutputStream();
			
			ImageIO.write(image, "png", imagebuffer);

			return new ByteArrayInputStream(imagebuffer.toByteArray());
	
		} catch (IOException e) {
			return null;
		}
	}
}
