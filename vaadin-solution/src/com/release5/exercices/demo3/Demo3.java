package com.release5.exercices.demo3;

import java.io.File;

import com.vaadin.server.ClassResource;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Image;

public class Demo3 extends GridLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	public Demo3() {
		
		super(3,2);

		setStyleAndLayout();

		addThemeResource();

		addFileResource();

		addClassResource();

		addStreamResource();
		
		addExternalResource();

	}

	private void addExternalResource() {

		String url = "http://img.lum.dolimg.com/v1/images/ep7_ia_162323_j_077412a0.jpeg?region=363%2C0%2C1165%2C654&width=320";
		
		ExternalResource resource = new ExternalResource(url);
		
		Image image = new Image("ExternalResource", resource);

		this.addComponent(image);
	}

	private void addThemeResource() {

		ThemeResource resource = new ThemeResource("img/yoda.jpg");

		Image image = new Image("ThemeResource", resource);

		this.addComponent(image);

	}

	private void addFileResource() {
		
		VaadinService vaadinService = VaadinService.getCurrent();

		String basepath = vaadinService.getBaseDirectory().getAbsolutePath();

		FileResource resource = new FileResource(new File(basepath + "/WEB-INF/img/vader.png"));

		Image image = new Image("FileResource", resource);
		
		addComponent(image);

	}

	private void addClassResource() {

		ClassResource resource = new ClassResource(Demo3.class, "solo.jpeg");

		Image image = new Image("ClassResource", resource);

		addComponent(image);

	}

	private void addStreamResource() {

		StreamSource imagesource = new DeathStarSource();

		StreamResource resource = new StreamResource(imagesource, "death-star.png");

		Image image = new Image("StreamResource", resource);

		addComponent(image);
	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);

	}

}
