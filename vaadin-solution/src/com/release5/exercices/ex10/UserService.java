package com.release5.exercices.ex10;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.release5.exercices.ex10.model.User;

public class UserService implements Serializable {

	/** IDE */
	private static final long serialVersionUID = 7089213398885677137L;

	public List<User> getUsers() {

		List<User> users = new ArrayList<User>();

		User userA = new User("Shantanu Narayen", "Marketing Magician");
		users.add(userA);

		User userB = new User("Brad Smith", "Social Media Rockstar");
		users.add(userB);

		User userC = new User("John Legere", "Digital Overlord");
		users.add(userC);

		return users;

	}

}
