package com.release5.exercices.ex10;

import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class Tab10 extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	private UserService service = new UserService();

	private UserLayoutDropWrapper availableUsers = new UserLayoutDropWrapper(service.getUsers());

	private UserLayoutDropWrapper blackListUsers = new UserLayoutDropWrapper(null);

	public Tab10() {

		setStyleAndLayout();

		addAvailableUsers();

		addBlackListedUsers();

	}

	private void addAvailableUsers() {

		Panel availableUsersPanel = new Panel();

		availableUsersPanel.setCaption("AVAILABLE");

		availableUsersPanel.setWidth(1000, Unit.PIXELS);

		availableUsersPanel.setContent(availableUsers);

		addComponent(availableUsersPanel);

		availableUsers.setOtherLayout(blackListUsers.getUserLayout());
	}

	private void addBlackListedUsers() {

		Panel blackListUsersPanel = new Panel();

		blackListUsersPanel.setCaption("BLACK LIST");

		blackListUsersPanel.setWidth(1000, Unit.PIXELS);

		blackListUsersPanel.setContent(blackListUsers);

		addComponent(blackListUsersPanel);

		blackListUsers.setOtherLayout(availableUsers.getUserLayout());
	}

	private void setStyleAndLayout() {

		setSpacing(true);

		setMargin(true);

	}

}
