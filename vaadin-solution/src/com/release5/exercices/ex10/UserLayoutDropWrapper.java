package com.release5.exercices.ex10;

import java.util.List;

import com.release5.exercices.ex10.model.User;
import com.vaadin.event.Transferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.ui.Component;
import com.vaadin.ui.DragAndDropWrapper;

class UserLayoutDropWrapper extends DragAndDropWrapper implements DropHandler {

	/** IDE. */
	private static final long serialVersionUID = 2633429864573483558L;

	private UserLayout userLayout;

	private UserLayout otherLayout;

	UserLayoutDropWrapper(List<User> users) {

		super(new UserLayout(users));

		this.userLayout = (UserLayout) super.getCompositionRoot();

		setDropHandler(this);

	}

	void setOtherLayout(UserLayout otherLayout) {

		this.otherLayout = otherLayout;

	}

	  UserLayout getUserLayout() {

		return userLayout;

	}

	@Override
	public AcceptCriterion getAcceptCriterion() {
		return AcceptAll.get();
	}

	@Override
	public void drop(DragAndDropEvent event) {

		Transferable transferable = event.getTransferable();

		if (transferable == null) {
			return;
		}

		Component sourceComponent = transferable.getSourceComponent();

		if (sourceComponent instanceof UserCardDropWrapper) {

			UserCardDropWrapper userCardDropWrapper = (UserCardDropWrapper) sourceComponent;

			User user = userCardDropWrapper.getUser();

			userLayout.addUser(user);

			otherLayout.removeUser(user);

		}
	}

}
