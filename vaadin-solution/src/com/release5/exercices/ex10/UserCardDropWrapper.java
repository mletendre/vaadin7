package com.release5.exercices.ex10;

import com.release5.exercices.ex10.model.User;
import com.vaadin.ui.DragAndDropWrapper;

class UserCardDropWrapper extends DragAndDropWrapper {

	/** IDE. */
	private static final long serialVersionUID = 2290976818554370848L;

	private User user;

	UserCardDropWrapper(User user) {

		super(new UserCard(user));

		this.user = user;

		setDragStartMode(DragStartMode.WRAPPER);
	}

	public User getUser() {
		return user;
	}

}
