package com.release5.exercices.ex10;

import com.release5.exercices.ex10.model.User;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class UserCard extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = -7367967795365230721L;

	private User user;

	UserCard(User user) {

		this.user = user;

		setStyleAndLayout();

		addName();

		addJobTitle();

	}

	private void addName() {
		Label label = new Label(FontAwesome.USER.getHtml() + " "
				+ user.getName(), ContentMode.HTML);
		this.addComponent(label);

	}

	private void addJobTitle() {

		Label label = new Label(user.getJobTitle());
		this.addComponent(label);

	}

	private void setStyleAndLayout() {

		setSpacing(true);

		setMargin(true);

		this.setWidth(250, Unit.PIXELS);

		setStyleName("user-card");

	}

	public User getUser() {
		return user;

	}

}
