	package com.release5.exercices.ex4;

import java.util.Date;

import com.release5.exercices.ExerciesTabSheet;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.DateRangeValidator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class Tab4 extends GridLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	private TextField firstNamefield = new TextField();

	private TextField lastNamefield = new TextField();

	private TextField emailField = new TextField();

	private PopupDateField hireDateField = new PopupDateField();

	private ExerciesTabSheet exerciesTabSheet;

	private static final String WELCOME_MESSAGE = "Entrer l'information S.V.P";

	private BeanItem<Person> personBeanItem = new BeanItem<Person>(new Person());

	private FieldGroup binder = new FieldGroup(personBeanItem);

	public Tab4(ExerciesTabSheet exerciesTabSheet) {

		super(4, 8);

		this.exerciesTabSheet = exerciesTabSheet;

		setStyleAndLayout();

		addFeedbackMessage(WELCOME_MESSAGE, FontAwesome.INFO_CIRCLE);

		addLeftSubtitle();

		addFirstName();

		addLastName();

		addEmail();

		addGender();

		addActivate();

		addRightSubtitle();

		addDepartment();

		addHireDate();

		addComment();

		addButtons();

	}

	private void addFeedbackMessage(String message, FontAwesome icon) {

		final int ROW = 0;

		this.removeComponent(0, ROW);

		Label label = new Label(icon.getHtml() + " " + message,
				ContentMode.HTML);
		addComponent(label, 0, ROW, 3, ROW);

	}

	private void addLeftSubtitle() {
		final int ROW = 1;
		Label label = new Label("Description");
		addComponent(label, 0, ROW);
	}

	private void addFirstName() {

		final int ROW = 2;
		Label label = new Label("Prénom:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		firstNamefield.setWidth(250, Unit.PIXELS);
		addComponent(firstNamefield, 1, ROW);
		binder.bind(firstNamefield, "firstName");

		firstNamefield
				.addValidator(new StringLengthValidator("", 2, 20, false));
	}

	private void addLastName() {
		final int ROW = 3;
		Label label = new Label("Nom:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		lastNamefield.setWidth(250, Unit.PIXELS);
		addComponent(lastNamefield, 1, ROW);
		binder.bind(lastNamefield, "lastName");

		lastNamefield.addValidator(new StringLengthValidator("", 2, 20, false));
	}

	private void addEmail() {
		final int ROW = 4;

		Label label = new Label("Courriel:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		emailField.setWidth(250, Unit.PIXELS);
		addComponent(emailField, 1, ROW);
		binder.bind(emailField, "mail");

		emailField.addValidator(new EmailValidator(""));

	}

	private void addGender() {
		final int ROW = 5;

		Label label = new Label("Sexe:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		OptionGroup optionGroup = new OptionGroup();
		addComponent(optionGroup, 1, ROW);
		binder.bind(optionGroup, "gender");

		optionGroup.addItem(true);
		optionGroup.setItemCaption(true, "Masculin");
		optionGroup.setItemIcon(true, FontAwesome.MALE);

		optionGroup.addItem(false);
		optionGroup.setItemIcon(false, FontAwesome.FEMALE);
		optionGroup.setItemCaption(false, "Féminin");

	}

	private void addActivate() {

		final int ROW = 6;

		Label label = new Label("Actif:");
		label.setSizeUndefined();
		addComponent(label, 0, ROW);
		setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		CheckBox checkBox = new CheckBox();
		checkBox.setValue(true);
		addComponent(checkBox, 1, ROW);
		binder.bind(checkBox, "active");
	}

	private void addRightSubtitle() {
		final int ROW = 1;
		Label label = new Label("Profile Corporatif");
		addComponent(label, 2, ROW);
	}

	private void addDepartment() {

		final int ROW = 2;
		Label label = new Label("Departement:");
		label.setSizeUndefined();
		addComponent(label, 2, ROW);
		setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		ComboBox comboBox = new ComboBox();
		comboBox.addItem("Finance");
		comboBox.addItem("Achats");
		comboBox.addItem("Ventes");
		comboBox.addItem("TI");
		comboBox.setNullSelectionAllowed(false);
		addComponent(comboBox, 3, ROW);
		binder.bind(comboBox, "department");

	}

	private void addHireDate() {

		final int ROW = 3;

		Label label = new Label("Date d'embauche:");
		label.setSizeUndefined();
		addComponent(label, 2, ROW);
		setComponentAlignment(label, Alignment.MIDDLE_RIGHT);

		addComponent(hireDateField, 3, ROW);
		binder.bind(hireDateField, "hireDate");

		hireDateField.addValidator(new DateRangeValidator("", null, new Date(),
				Resolution.DAY));

	}

	private void addComment() {

		final int ROW = 4;

		Label label = new Label("Commentaire:");
		label.setSizeUndefined();
		addComponent(label, 2, ROW);
		setComponentAlignment(label, Alignment.TOP_RIGHT);

		TextArea commentField = new TextArea();
		commentField.setSizeFull();
		addComponent(commentField, 3, ROW);
		binder.bind(commentField, "comment");
		
	}

	private void addButtons() {

		final int ROW = 5;

		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		this.addComponent(buttonLayout, 3, ROW);
		this.setComponentAlignment(buttonLayout, Alignment.TOP_RIGHT);

		addClearButton(buttonLayout);

		addSaveButton(buttonLayout);

	}

	private void addSaveButton(HorizontalLayout buttonLayout) {

		Button button = new Button("Save");
		buttonLayout.addComponent(button);

		button.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = 609980225038593221L;

			@Override
			public void buttonClick(ClickEvent event) {

				save();

			}

		});

	}

	private void save() {

		boolean valid = validate();

		if (valid) {

			try {

				binder.commit();

				addFeedbackMessage("The user was added: "
						+ personBeanItem.getBean().toString(),
						FontAwesome.CHECK);

			} catch (CommitException e) {

				e.printStackTrace();

				addFeedbackMessage("System error call administrator",
						FontAwesome.BOMB);
			}

		}
	}

	private boolean validate() {

		if (!firstNamefield.isValid()) {

			addFeedbackMessage("First name must have 2 to 20 characters",
					FontAwesome.WARNING);
			return false;

		} else if (!lastNamefield.isValid()) {

			addFeedbackMessage("Last name must have 2 to 20 characters",
					FontAwesome.WARNING);
			return false;

		} else if (!emailField.isValid()) {

			addFeedbackMessage("Email address must be valid !",
					FontAwesome.WARNING);
			return false;

		} else if (!hireDateField.isValid()) {

			addFeedbackMessage("The hire date can not be in the future !",
					FontAwesome.WARNING);
			return false;
		}

		return true;
	}

	private void addClearButton(HorizontalLayout buttonLayout) {

		Button button = new Button("Clear");
		buttonLayout.addComponent(button);

		button.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = 3395041117459414843L;

			@Override
			public void buttonClick(ClickEvent event) {

				clear();

			}
		});

	}

	protected void clear() {

		Tab4 newTab = new Tab4(exerciesTabSheet);

		exerciesTabSheet.replaceComponent(this, newTab);
	}

	private void setStyleAndLayout() {

		setWidth(100, Unit.PERCENTAGE);

		setSpacing(true);

		setMargin(true);
	}

}
