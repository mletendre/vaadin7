package com.release5.exercices.ex4;

import java.io.Serializable;
import java.util.Date;

public class Person implements Serializable {

	/** IDE. */
	private static final long serialVersionUID = -8676080842623577668L;

	private String firstName = "";

	private String lastName = "";

	private String mail = "";

	private boolean gender = true;

	private boolean active = true;

	private String department = "Finance";

	private Date hireDate = new Date();

	private String comment = "";

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName
				+ ", mail=" + mail + ", gender=" + gender + ", active="
				+ active + ", department=" + department + ", hireDate="
				+ hireDate + ", comment=" + comment + "]";
	}

}
