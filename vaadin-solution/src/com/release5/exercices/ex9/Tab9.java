package com.release5.exercices.ex9;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

public class Tab9 extends HorizontalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	private TextField field = new TextField();

	public Tab9() {

		setStyleAndLayout();

		addThemeOption();

		addColorOption();

		addTextColorOption();

		addComponent(field);

	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);

	}

	private void addColorOption() {
		OptionGroup colorOption = new OptionGroup("ARRIÈRE PLAN");
		colorOption.addItem("none");
		colorOption.addItem("red");
		colorOption.addItem("blue");
		colorOption.addItem("orange");
		colorOption.select("none");
		addComponent(colorOption);

		colorOption.addValueChangeListener(new ValueChangeListener() {

			/** IDE. */
			private static final long serialVersionUID = 8105190624430001774L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Object value = event.getProperty().getValue();

				if (value != null && value instanceof String) {

					String color = (String) value;

					String stylesExpressions = field.getStyleName();

					String[] styles = StringUtils.split(stylesExpressions);

					for (String styleName : styles) {

						if (styleName != null && styleName.startsWith("back-")) {
							field.removeStyleName(styleName);
						}

					}

					field.addStyleName("back-" + color);

				}
			}
		});

	}

	private void addTextColorOption() {
		OptionGroup textColorOption = new OptionGroup("TEXTE");

		textColorOption.addItem("black");
		textColorOption.addItem("white");
		textColorOption.addItem("yellow");
		textColorOption.select("black");
		addComponent(textColorOption);

		textColorOption.addValueChangeListener(new ValueChangeListener() {

			/** IDE. */
			private static final long serialVersionUID = 4969350406186595994L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Object value = event.getProperty().getValue();

				if (value != null && value instanceof String) {

					String color = (String) value;

					String stylesExpressions = field.getStyleName();

					String[] styles = StringUtils.split(stylesExpressions);

					for (String styleName : styles) {

						if (styleName != null && styleName.startsWith("text-")) {
							field.removeStyleName(styleName);
						}

					}

					field.addStyleName("text-" + color);

				}
			}
		});

	}

	private void addThemeOption() {

		OptionGroup themeOption = new OptionGroup("THEME");
		themeOption.addItem("valo-2.0");
		themeOption.addItem("reindeer-2.0");
		themeOption.addItem("runo-2.0");
		themeOption.select("valo-2.0");
		addComponent(themeOption);

		themeOption.addValueChangeListener(new ValueChangeListener() {

			/** IDE. */
			private static final long serialVersionUID = -1008196347448705557L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Object value = event.getProperty().getValue();

				if (value != null && value instanceof String) {

					String newTheme = (String) value;

					UI.getCurrent().setTheme(newTheme);

				}
			}
		});

	}

}
