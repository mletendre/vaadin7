package com.release5.exercices.ex7.model;

import java.io.Serializable;

public class Superhero implements Serializable {

	/** IDE. */
	private static final long serialVersionUID = 5287518670886298805L;

	private int id;

	private boolean vilain;

	private Group group;

	private String name;

	public Superhero(int id, boolean vilain, Group group, String name) {
		super();
		this.id = id;
		this.vilain = vilain;
		this.group = group;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public boolean isVilain() {
		return vilain;
	}

	public Group getGroup() {
		return group;
	}

	public String getName() {
		return name;
	}

}
