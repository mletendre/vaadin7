package com.release5.exercices.ex7;

import java.util.List;

import com.release5.exercices.ex7.model.Group;
import com.release5.exercices.ex7.model.Superhero;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.HierarchicalContainer;

class HeroHierarchicalContainer extends HierarchicalContainer {

	/** IDE. */
	private static final long serialVersionUID = 1026613672445531942L;

	private HeroesService heroesService = new HeroesService();

	HeroHierarchicalContainer() {

		super();

		loadData();

	}

	Superhero addHero(Integer groupId, String name, boolean isVilain) {

		Superhero superhero = heroesService.addSuperhero(groupId, name,
				isVilain);

		addHero(superhero, superhero.getGroup());

		return superhero;

	}

	boolean isGroup(Integer id) {
		return heroesService.isGroup(id);
	}

	private void loadData() {

		List<Group> heroes = heroesService.getHeroes();

		addContainerProperty("node", Object.class, "");

		addContainerProperty("caption", String.class, "");

		for (Group group : heroes) {

			addGroup(group, null);
		}

	}

	private void addGroup(Group group, Group parent) {

		int id = group.getId();

		Item groupItem = addItem(id);

		groupItem.getItemProperty("node").setValue(group);

		groupItem.getItemProperty("caption").setValue(group.getName());

		List<Superhero> heroes = group.getHeroes();

		for (Superhero hero : heroes) {

			addHero(hero, group);
		}

		List<Group> childGroups = group.getChildGroups();

		for (Group childGroup : childGroups) {

			addGroup(childGroup, group);
		}

		if (parent != null) {

			setParent(id, parent.getId());
		}

	}

	private void addHero(Superhero hero, Group parent) {

		int id = hero.getId();

		Item heroItem = addItem(id);

		Property nodeItemProperty = heroItem.getItemProperty("node");

		nodeItemProperty.setValue(hero);

		heroItem.getItemProperty("caption").setValue(hero.getName());

		if (parent != null) {

			setParent(id, parent.getId());
		}

		setChildrenAllowed(id, false);

	}

}
