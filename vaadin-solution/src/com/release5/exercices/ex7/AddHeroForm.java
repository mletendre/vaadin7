package com.release5.exercices.ex7;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

class AddHeroForm extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = -6690347884687356660L;

	final TextField nameTextField = new TextField("Entrer le nom:");

	private CheckBox vilainCheckBox = new CheckBox("Vilain ?", false);

	AddHeroForm(final AddHeroWindow window, final HeroesTree tree) {

		setStyleAndLayout();

		addNameField();

		addVilainField();

		addButton(window, tree);

	}

	private void addButton(final AddHeroWindow window, final HeroesTree tree) {

		Button button = new Button("Ajouter");

		button.setIcon(FontAwesome.PLUS_CIRCLE);

		addComponent(button);

		setComponentAlignment(button, Alignment.MIDDLE_CENTER);

		button.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = 7303576896764495715L;

			@Override
			public void buttonClick(ClickEvent event) {

				tree.addHero(nameTextField.getValue(),
						vilainCheckBox.getValue());

				window.close();
			}
		});
	}

	private void addNameField() {

		addComponent(nameTextField);

		setComponentAlignment(nameTextField, Alignment.MIDDLE_CENTER);
	}

	private void addVilainField() {

		addComponent(vilainCheckBox);

		setComponentAlignment(vilainCheckBox, Alignment.MIDDLE_CENTER);

	}

	private void setStyleAndLayout() {

		setSpacing(true);

		setMargin(true);

		setHeight(180, Unit.PIXELS);

		this.setWidth(250, Unit.PIXELS);
	}

}
