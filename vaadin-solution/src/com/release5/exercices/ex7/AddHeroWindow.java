package com.release5.exercices.ex7;

import com.vaadin.ui.Window;

class AddHeroWindow extends Window {

	/** IDE. */
	private static final long serialVersionUID = 6854335055429917797L;

	AddHeroWindow(HeroesTree tree) {

		setModal(true);

		center();

		AddHeroForm content = new AddHeroForm(this,tree);

		this.setContent(content);

	}

}
