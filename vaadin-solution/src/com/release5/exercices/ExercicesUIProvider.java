package com.release5.exercices;

import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.UIProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

public class ExercicesUIProvider extends UIProvider {

	/** IDE. */
	private static final long serialVersionUID = -7997254090208409622L;

	@Override
	public Class<? extends UI> getUIClass(UIClassSelectionEvent event) {

		VaadinRequest request = event.getRequest();

		String header = request.getHeader("user-agent");

		if (header.startsWith("Lynx")) {

			return LynxUI.class;
		}

		return VaadinExercicesUI.class;
	}

}
