package com.release5.exercices.demo1;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class Demo1 extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	private SecurityManager securityManager;

	private TextField userTextField = new TextField("User:");

	private PasswordField passwTextField = new PasswordField("Password:");

	private Label label = new Label("");

	public Demo1() {

		setStyleAndLayout();

		loadShiro();

		addWelcomeMessage();

		addUserName();

		addPassword();

		addButton();

	}

	private void addWelcomeMessage() {

		label.setIcon(FontAwesome.INFO);

		addComponent(label);

		label.setCaption("Please enter username/password.");

	}

	private void addUserName() {

		userTextField.setIcon(FontAwesome.USER);

		this.addComponent(userTextField);
	}

	private void addPassword() {

		passwTextField.setIcon(FontAwesome.STAR);

		this.addComponent(passwTextField);
	}

	private void addButton() {
		Button button = new Button("LOGIN");

		button.setIcon(FontAwesome.SIGN_IN);

		addComponent(button);

		button.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = -641032428481625002L;

			@Override
			public void buttonClick(ClickEvent event) {

				authenticate();

			}
		});

	}

	private void authenticate() {

		Subject currentUser = SecurityUtils.getSubject();

		if (!currentUser.isAuthenticated()) {

			String username = userTextField.getValue();

			String password = passwTextField.getValue();

			UsernamePasswordToken token = new UsernamePasswordToken(username, password);

			token.setRememberMe(true);

			try {

				currentUser.login(token);

				authorize(currentUser);

			} catch (UnknownAccountException uae) {

				String message = "There is no user with username of " + token.getPrincipal();

				label.setCaption(message);
				
				label.setIcon(FontAwesome.BOMB);


			} catch (IncorrectCredentialsException ice) {

				String message = "Password for account " + token.getPrincipal() + " was incorrect!";

				label.setCaption(message);
				
				label.setIcon(FontAwesome.BOMB);

			} catch (LockedAccountException lae) {

				String message = "The account for username " + token.getPrincipal() + " is locked.  "
						+ "Please contact your administrator to unlock it.";

				label.setCaption(message);
				
				label.setIcon(FontAwesome.BOMB);

			}
		}

	}

	private void authorize(Subject currentUser) {

		String hasViewRole = currentUser.hasRole("view_role") ? "view" : "";

		String hasEditRole = currentUser.hasRole("edit_role") ? "edit" : "";

		String message = currentUser.getPrincipal() + "  user has these prermissions:  " + hasViewRole + " "
				+ hasEditRole;

		label.setCaption(message);

	}

	private void loadShiro() {

		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");

		securityManager = factory.getInstance();

		SecurityUtils.setSecurityManager(securityManager);
	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);

	}

}
