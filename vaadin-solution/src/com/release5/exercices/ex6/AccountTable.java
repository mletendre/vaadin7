package com.release5.exercices.ex6;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Image;
import com.vaadin.ui.Table;

class AccountTable extends Table {

	/** IDE. */
	private static final long serialVersionUID = -7802110227143612458L;

	private BeanItemContainer<Account> container = new BeanItemContainer<Account>(
			Account.class);

	private static final ThemeResource USD_ICON = new ThemeResource(
			"img/USD.png");

	private static final ThemeResource CAD_ICON = new ThemeResource(
			"img/CAD.png");

	private final static Action REMOVE_ACTION = new Action("Supprimer",
			FontAwesome.MINUS_CIRCLE);

	AccountTable() {
		List<Account> accounts = getAccounts();
		container.addAll(accounts);
		setContainerDataSource(container);
		setPageLength(5);
		setVisibleColumns(new String[] { "number", "currency", "currentBalance" });
		setColumnHeaders(new String[] { "Numéro de compte", "Devise", "Solde" });
		setColumnReorderingAllowed(true);
		setColumnAlignment("number", Align.RIGHT);
		setColumnWidth("number", 300);
		setFooterVisible(true);
		setColumnFooter("currentBalance", "20 000");
		setColumnFooter("currency", "USD");
		setColumnFooter("number", "Total:");

		addGeneratedColumn("currency", new ColumnGenerator() {

			/** IDE. */
			private static final long serialVersionUID = -1814653625145963175L;

			public Object generateCell(Table source, Object itemId,
					Object columnId) {
				if (itemId instanceof Account) {
					Account Account = (Account) itemId;
					String currency = Account.getCurrency();
					if (currency.equals("CAD")) {
						return new Image(null, CAD_ICON);
					} else if (currency.equals("USD")) {
						return new Image(null, USD_ICON);
					}
				}
				return new Image(null, null);
			}
		});
		
		addActionHandler(new Handler() {

			/** IDE. */
			private static final long serialVersionUID = 3077982569939993480L;

			public void handleAction(Action action, Object sender, Object target) {
				if (action == REMOVE_ACTION) {
					container.removeItem(target);
					
					setColumnFooter("currentBalance", "-1");
				}
			}

			public Action[] getActions(Object target, Object sender) {
				return new Action[] { REMOVE_ACTION };
			}
		});
	}

	private List<Account> getAccounts() {

		List<Account> accounts = new ArrayList<Account>();

		accounts.add(new Account("4464666", "USD", 1000l, 1100l));
		accounts.add(new Account("4454554", "CAD", 2000l, 1100l));
		accounts.add(new Account("9987555", "USD", 3000l, 3500l));
		accounts.add(new Account("11235444", "CAD", 800l, 750l));
		accounts.add(new Account("87835444", "USD", 800l, 750l));

		accounts.add(new Account("4464666", "CAD", 1000l, 1100l));
		accounts.add(new Account("4454554", "USD", 2000l, 321231l));
		accounts.add(new Account("545", "USD", 3000l, 3500l));
		accounts.add(new Account("488785444", "USD", 800l, 750l));
		accounts.add(new Account("888887", "CAD", 800l, 750l));

		return accounts;

	}
}
