package com.release5.exercices.demo4;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

public class Demo4 extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	private NativeSelect planetSelect;

	private Button button;

	public Demo4() {

		setStyleAndLayout();

		Design.read("Demo4.html", this);

		button.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = 8853769919544094573L;

			@Override
			public void buttonClick(ClickEvent event) {

				String message = "Hello " + planetSelect.getValue() + "ling";

				Notification.show(message);
			}

		});
		
		addComponent(new TimeInput());

	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);

	}

}
