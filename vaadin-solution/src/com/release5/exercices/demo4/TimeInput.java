package com.release5.exercices.demo4;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

public class TimeInput extends HorizontalLayout {

	public TimeInput() {

		setSpacing(true);

		TextField hours = new TextField();
		hours.setValue("12");
		hours.setMaxLength(2);
		hours.setWidth(50, Unit.PIXELS);
		this.addComponent(hours);

		addComponent(new Label(":"));

		TextField minutes = new TextField();
		minutes.setValue("00");
		minutes.setMaxLength(2);
		minutes.setWidth(50, Unit.PIXELS);
		this.addComponent(minutes);
	}

}
