package com.release5.exercices.demo2;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

public class Demo2 extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = 7066221840051172419L;

	public Demo2() {

		setStyleAndLayout();

		VaadinRequest currentRequest = VaadinService.getCurrentRequest();

		String contextPath = currentRequest.getContextPath();

		Link link = new Link("Show PDF Report", new ExternalResource(contextPath + "/pdf/"));

		this.addComponent(link);
	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);

	}

}
