package com.release5.exercices.demo2	;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.vaadin.server.RequestHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinSession;

public class PdfRequestHandler implements RequestHandler {

	/** IDE. */
	private static final long serialVersionUID = 5514810419722539378L;

	@Override
	public boolean handleRequest(VaadinSession session, VaadinRequest request, VaadinResponse response)
			throws IOException {

		String pathInfo = request.getPathInfo();

		if ("/pdf/".equals(pathInfo)) {

			ByteArrayOutputStream outputStream = getPDF();

			byte[] pdfDocument = outputStream.toByteArray();

			response.setContentType("application/pdf");

			OutputStream output = response.getOutputStream();

			output.write(pdfDocument);

			output.close();

		}
		return false;
	}

	private ByteArrayOutputStream getPDF() {

		PDDocument document = new PDDocument();

		ByteArrayOutputStream output = new ByteArrayOutputStream();

		PDPage blankPage = new PDPage();

		document.addPage(blankPage);

		try {

			PDPageContentStream contents = new PDPageContentStream(document, blankPage);
			contents.beginText();
			contents.setFont(PDType1Font.HELVETICA_BOLD, 12);
			contents.setNonStrokingColor(Color.BLACK);
			contents.moveTextPositionByAmount(20, 500);
			contents.drawString("Vaadin is an open source Web application framework for rich Internet applications.");
			contents.endText();
			contents.close();

			document.save(output);
		} catch (COSVisitorException | IOException e) {
			e.printStackTrace();
		}

		return output;

	}

}
