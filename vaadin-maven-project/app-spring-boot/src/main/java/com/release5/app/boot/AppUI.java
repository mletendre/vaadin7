package com.release5.app.boot;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 *
 */
@Theme("theme-blue")
@SpringUI
public class AppUI extends UI {

	/** IDE. */
	private static final long serialVersionUID = 8717148934446616739L;

	@Autowired
	private SpringViewProvider viewProvider;

	@WebListener
	public static class MyContextLoaderListener extends ContextLoaderListener {
	}

	@WebServlet(value = "/*", asyncSupported = true)
	public static class Servlet extends SpringVaadinServlet {

		/** IDE. */
		private static final long serialVersionUID = -2886625470808739508L;
	}

	@Configuration
	@EnableVaadin
	public static class MyConfiguration {
	}

	@Override
	protected void init(VaadinRequest request) {
		final VerticalLayout root = new VerticalLayout();
		root.setSizeFull();
		setContent(root);

		Navigator navigator = new Navigator(this, root);
		navigator.addProvider(viewProvider);
	}

}
