package com.release5.app.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.release5.services", "com.release5.app.boot" })
public class Application {

	public static void main(String[] args) {
		
		SpringApplication.run(Application.class, args);
	
	}
}
