package com.release5.services;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class QuoteService {

	private Random random = new Random();

	private String[] quotes = new String[] { "Would it save you a lot of time if I just gave up and went mad now?",
			"Time is an illusion. Lunchtime doubly so.",
			"Space is big. You just won't believe how vastly, hugely, mind-bogglingly big it is. I mean, you may think it's a long way down the road to the chemist's, but that's just peanuts to space.",
			"If there's anything more important than my ego around, I want it caught and shot now.",
			"I'd far rather be happy than right any day.", "Don't Panic." };

	public String getQuote() {

		int index = random.nextInt(quotes.length + 1);

		return quotes[index - 1];
	}

}
