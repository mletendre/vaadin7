# Credits
[Release 5 inc.](http://www.release5.com/)


# Description

This projet provide full example of a Vaadin Enterprise project. 

# Compilation

```
cd vaadin-maven-project
mvn clean install
```

# Execution

## Execute the spring without spring boot application (with Jetty) but with XML configuration

```
cd vaadin-maven-project/app-spring-xml
mvn jetty:run
```

## Execute the spring without spring boot application (with Jetty) but with Annotation configuration

```
cd vaadin-maven-project/app-spring-annotation
mvn jetty:run
```

# Execute the spring boot application.

```
cd vaadin-maven-project/app-spring-boot
mvn spring-boot:run
```

# Execute the google  guice application

```
cd vaadin-maven-project/app-guice
mvn jetty:run
```

