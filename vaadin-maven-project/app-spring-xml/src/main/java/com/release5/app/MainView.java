package com.release5.app;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.release5.component.captcha.CaptchaField;
import com.release5.services.QuoteService;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = MainView.VIEW_NAME)
class MainView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "";

	/** IDE. */
	private static final long serialVersionUID = -4445667367123687193L;

	@Autowired
	private QuoteService quoteService;

	@PostConstruct
	void init() {

		setStyleAndLayout();

		addTitle();

		addStyleComboBox();

		addCaptcha();

		addQuoteButton();

	}

	@Override
	public void enter(ViewChangeEvent event) {

	}

	private void addTitle() {

		Label label = new Label("&nbsp;Vaadin Application: 1-without Spring Boot, 2- with XML configuration");

		label.addStyleName("my-label");

		label.setContentMode(ContentMode.HTML);

		addComponent(label);
	}

	private void addStyleComboBox() {

		OptionGroup option = new OptionGroup();

		option.addItem("theme-blue");
		option.setItemCaption("theme-blue", "BLUE");

		option.addItem("theme-red");
		option.setItemCaption("theme-red", "RED");

		option.select("theme-blue");

		this.addComponent(option);

		option.addValueChangeListener(new ValueChangeListener() {

			/** IDE. */
			private static final long serialVersionUID = 6558297507858900510L;

			public void valueChange(ValueChangeEvent event) {

				String theme = (String) event.getProperty().getValue();

				UI ui = UI.getCurrent();

				ui.setTheme(theme);

			}
		});

	}

	private void addCaptcha() {

		CaptchaField captchaField = new CaptchaField();

		this.addComponent(captchaField);

	}

	private void addQuoteButton() {

		Button quoteButton = new Button("Display Quote");

		quoteButton.addClickListener(new ClickListener() {

			/** IDE. */
			private static final long serialVersionUID = 1967821383325370148L;

			@Override
			public void buttonClick(final ClickEvent event) {

				Notification.show(quoteService.getQuote(), Type.TRAY_NOTIFICATION);
			}
		});

		this.addComponent(quoteButton);
	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);
	}

}
