package com.release5.showcase;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

/**
 *
 */
@Theme("showcase")
@Widgetset("com.release5.widgetset.Release5WidgetSet")
public class ShowcaseUI extends UI {

	/** IDE. */
	private static final long serialVersionUID = 8717148934446616739L;

	private final ShowcaseTabSheet content = new ShowcaseTabSheet();

	@Override
	protected void init(VaadinRequest vaadinRequest) {

		setContent(content);

	}

	@WebServlet(urlPatterns = "/*", name = "AppUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = ShowcaseUI.class, productionMode = false)
	public static class AppUIServlet extends VaadinServlet {

		/** IDE. */
		private static final long serialVersionUID = 8524915631137982466L;
	}
}
