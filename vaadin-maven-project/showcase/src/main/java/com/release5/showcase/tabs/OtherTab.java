package com.release5.showcase.tabs;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class OtherTab extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = -4445667367123687193L;

	public OtherTab() {

		setStyleAndLayout();

		addDescription();

		addCode();

		addExample();

	}

	private void addDescription() {
		Label title = new Label("Description");
		title.addStyleName("strong");
		addComponent(title);

		Label description = new Label("Description of other component");
		addComponent(description);
	}

	private void addCode() {
		Label title = new Label("Source Code");
		title.addStyleName("strong");
		addComponent(title);

		Label description = new Label("code here...");
		addComponent(description);
	}

	private void addExample() {
		Label title = new Label("Example");
		title.addStyleName("strong");
		addComponent(title);

		Label description = new Label("a live example here...");
		addComponent(description);

	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);
	}

}
