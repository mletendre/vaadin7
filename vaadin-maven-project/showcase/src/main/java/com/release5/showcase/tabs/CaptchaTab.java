package com.release5.showcase.tabs;

import com.release5.component.captcha.CaptchaField;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class CaptchaTab extends VerticalLayout {

	/** IDE. */
	private static final long serialVersionUID = -4445667367123687193L;

	public CaptchaTab() {

		setStyleAndLayout();

		addDescription();

		addCode();

		addExample();

	}

	private void addDescription() {
		Label title = new Label("Description");
		title.addStyleName("strong");
		addComponent(title);

		Label description = new Label(
				"A CAPTCHA (an acronym for Completely Automated Public Turing test to tell Computers and Humans Apart) is a type of challenge-response test used in computing to determine whether or not the user is human.");
		addComponent(description);
	}

	private void addCode() {
		Label title = new Label("Source Code");
		title.addStyleName("strong");
		addComponent(title);

		Label description = new Label(
				"CaptchaField captchaField = new CaptchaField();");
		addComponent(description);
	}

	private void addExample() {
		Label title = new Label("Example");
		title.addStyleName("strong");
		addComponent(title);

		CaptchaField captchaField = new CaptchaField();
		addComponent(captchaField);

	}

	private void setStyleAndLayout() {

		setMargin(true);

		setSpacing(true);
	}

}
