package com.release5.showcase;

import com.release5.showcase.tabs.CaptchaTab;
import com.release5.showcase.tabs.OtherTab;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.TabSheet;

class ShowcaseTabSheet extends TabSheet {

	/** IDE. */
	private static final long serialVersionUID = 8731375085540682472L;

	ShowcaseTabSheet() {
		addTab(new CaptchaTab(), "Captcha", FontAwesome.SHIELD);

		addTab(new OtherTab(), "Other 1", FontAwesome.QUESTION);

		addTab(new OtherTab(), "Other 2", FontAwesome.QUESTION);

	}

}
