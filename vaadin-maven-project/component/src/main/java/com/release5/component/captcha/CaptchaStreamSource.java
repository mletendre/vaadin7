package com.release5.component.captcha;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import cn.apiclub.captcha.Captcha;

import com.vaadin.server.StreamResource.StreamSource;

class CaptchaStreamSource implements StreamSource {

	/** Generated by IDE. */
	private static final long serialVersionUID = 1L;

	private Captcha captcha;

	CaptchaStreamSource(int width, int height) {

		captcha = new Captcha.Builder(width, height).addText().addBackground()
				.addNoise().addBorder().build();

	}

	public InputStream getStream() {

		BufferedImage image = captcha.getImage();

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		try {

			ImageIO.write(image, "png", buffer);

		} catch (IOException e) {

			return null;
		}

		return new ByteArrayInputStream(buffer.toByteArray());

	}

	public String getAnswer() {

		return captcha.getAnswer();
	}

}
